package com.bontech.carrental.initial;

import com.bontech.carrental.security.HashGenerator;
import com.bontech.carrental.user.UserEntity;
import com.bontech.carrental.user.UserRepository;
import com.bontech.carrental.user.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

import javax.annotation.PostConstruct;

@Component
public class InitialApplication {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;
    @Autowired
    HashGenerator generator;

    @PostConstruct
    public void registerAdmin() {
        if (!userService.userExistByUserName("bontech")) {
            UserEntity user = new UserEntity();
            user.setUserName("bontech");
            user.setPassword(generator.md5("hrm@bontech.ir"));
            user.setRole(UserEntity.Role.ADMIN);
            user.setCreatedAt(new Date());
            user.setUpdatedAt(new Date());
            user.setDeleted(false);

            userRepository.save(user);
        }
    }
}
