package com.bontech.carrental.security;

import java.util.regex.Pattern;

public class PasswordChecker {
    public static boolean checkRegex(String password) {

        Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
        Pattern lowerCasePatten = Pattern.compile("[a-z ]");
        Pattern digitCasePatten = Pattern.compile("[0-9 ]");

        boolean flag = true;

        if (password.length() < 8) {
            flag = false;
        }
        if (!UpperCasePatten.matcher(password).find()) {
            flag = false;
        }
        if (!lowerCasePatten.matcher(password).find()) {
            flag = false;
        }
        if (!digitCasePatten.matcher(password).find()) {
            flag = false;
        }

        return flag;
    }
}
