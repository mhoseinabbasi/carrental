package com.bontech.carrental.user;

import com.bontech.carrental.exceptionHandling.ExistedException;
import com.bontech.carrental.exceptionHandling.InternalServerException;
import com.bontech.carrental.exceptionHandling.MinistrationsNotFoundException;
import com.bontech.carrental.exceptionHandling.NotPermissionException;
import com.bontech.carrental.exceptionHandling.UserNotFoundException;
import com.bontech.carrental.history.HistoryEntity;
import com.bontech.carrental.history.HistoryService;
import com.bontech.carrental.ministration.MinistrationEntity;
import com.bontech.carrental.ministration.MinistrationService;
import com.bontech.carrental.security.HashGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;


@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    HashGenerator generator;
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    HistoryService historyService;
    @Autowired
    MinistrationService ministrationService;

    public UserEntity createClient(String userName, String password, Integer credit) {
        if (userExistByUserName(userName)) {
            throw new ExistedException("user", String.format("user name with name %s existed", userName));
        }

        try {
            password = generator.md5(password);
        } catch (Exception e) {
            throw new InternalServerException();
        }

        UserEntity userEntity = new UserEntity(userName, password);
        userEntity.setRole(UserEntity.Role.CLIENT);
        userEntity.setCredit(credit);
        userEntity.setCreatedAt(new Date());
        userEntity.setUpdatedAt(new Date());
        userEntity.setDeleted(false);

        return userRepository.save(userEntity);
    }

    public UserEntity createAdmin(String userName, String password) {
        if (userExistByUserName(userName)) {
            throw new ExistedException("user", String.format("user name with name %s existed", userName));
        }

        try {
            password = generator.md5(password);
        } catch (Exception e) {
            throw new InternalServerException();
        }

        UserEntity userEntity = new UserEntity(userName, password);
        userEntity.setRole(UserEntity.Role.ADMIN);
        userEntity.setCreatedAt(new Date());
        userEntity.setUpdatedAt(new Date());
        userEntity.setDeleted(false);

        return userRepository.save(userEntity);
    }

    public void licensing(String id, String ministrationId) {
        if (!userExistById(id)) {
            throw new UserNotFoundException(id);
        }

        if (!ministrationService.ministrationExistById(ministrationId)) {
            throw new MinistrationsNotFoundException(ministrationId);
        }

        UserEntity userEntity = userRepository.findByIdAndIsDeletedFalse(id);

        List<String> ministrationsId = new ArrayList<>();

        if (userEntity.getMinistrationsId() != null) {
            ministrationsId = userEntity.getMinistrationsId();
        }

        ministrationsId.add(ministrationId);
        userEntity.setMinistrationsId(ministrationsId);
        userEntity.setUpdatedAt(new Date());

        userRepository.save(userEntity);
    }

    public void cancelLicensing(String id, String ministrationId) {
        if (!userExistById(id)) {
            throw new UserNotFoundException(id);
        }

        if (!ministrationService.ministrationExistById(ministrationId)) {
            throw new MinistrationsNotFoundException(ministrationId);
        }

        UserEntity userEntity = userRepository.findByIdAndIsDeletedFalse(id);

        if (userEntity.getMinistrationsId() == null) {
            throw new MinistrationsNotFoundException();
        }

        userEntity.getMinistrationsId().remove(ministrationId);
        userEntity.setUpdatedAt(new Date());

        userRepository.save(userEntity);
    }

    public void useMinistration(String id, String ministrationId) {
        if (!ministrationService.ministrationExistById(ministrationId)) {
            throw new MinistrationsNotFoundException(ministrationId);
        }

        MinistrationEntity ministrationEntity = ministrationService.getMinistration(ministrationId);
        UserEntity userEntity = userRepository.findByIdAndIsDeletedFalse(id);

        List<String> ministrations = userEntity.getMinistrationsId();

        if (ministrations == null) {
            throw new NotPermissionException();
        }

        IntStream.range(0, ministrations.size()).filter(i -> !ministrations.contains(ministrationId)).forEach(i -> {
            throw new NotPermissionException();
        });


        if (ministrationService.getMinistration(ministrationId).getStatus() == MinistrationEntity.Status.OFF) {
            throw new NotPermissionException();
        }

        if (userEntity.getHistoryId() == null || !historyService.isToday(userEntity.getHistoryId())) {
            String historyId = historyService.create(ministrationEntity.getId(), id, ministrationEntity.getCostOfUse());
            userEntity.setHistoryId(historyId);
            userRepository.save(userEntity);
        }

        if (!isPermittedForUse(id, ministrationId)) {
            throw new NotPermissionException();
        }

        historyService.update(userEntity.getHistoryId(), historyService.getHistoryById(userEntity.getHistoryId()).getCountOfUse());

        Integer credit = userEntity.getCredit() - ministrationService.getMinistration(ministrationId).getCostOfUse();
        userEntity.setCredit(credit);

        userRepository.save(userEntity);
    }

    public UserEntity getUser(String id) {
        if (!userExistById(id)) {
            throw new UserNotFoundException(id);
        }

        return userRepository.findByIdAndIsDeletedFalse(id);
    }


    public List<UserEntity> getUsers(Integer skip, Integer limit) {
        Query query = new Query(Criteria.where("isDeleted").is(false));

        List<UserEntity> userEntityList = mongoTemplate.find(query.skip(skip).limit(limit), UserEntity.class);

        if (userEntityList.size() == 0) {
            throw new UserNotFoundException();
        }

        return userEntityList;
    }

    public UserEntity changeCredit(String id, Integer credit) {
        if (!userExistById(id)) {
            throw new UserNotFoundException(id);
        }

        UserEntity findUserEntity = userRepository.findByIdAndIsDeletedFalse(id);

        findUserEntity.setCredit(credit);
        findUserEntity.setUpdatedAt(new Date());

        return userRepository.save(findUserEntity);
    }

    public List<HistoryEntity> getUserHistories(String id) {
        if (!userExistById(id)) {
            throw new UserNotFoundException(id);
        }

        return historyService.getHistoriesByUserId(id);
    }

    public List<HistoryEntity> getHistories() {
        return historyService.getHistories();
    }

    public void delete(String id) {
        if (!userExistById(id)) {
            throw new UserNotFoundException(id);
        }

        UserEntity findUserEntity = userRepository.findByIdAndIsDeletedFalse(id);

        findUserEntity.setDeleted(true);

        userRepository.save(findUserEntity);
    }

    public List<String> getUserDuties(String id) {
        if (!userExistById(id)) {
            throw new UserNotFoundException(id);
        }

        UserEntity userEntity = userRepository.findByIdAndIsDeletedFalse(id);

        if (userEntity.getMinistrationsId() == null) {
            throw new MinistrationsNotFoundException();
        }

        return userEntity.getMinistrationsId();
    }

    public boolean isPermittedForUse(String id, String ministrationId) {
        UserEntity userEntity = userRepository.findByIdAndIsDeletedFalse(id);
        MinistrationEntity ministrationEntity = ministrationService.getMinistration(ministrationId);

        if (userEntity.getCredit() < ministrationEntity.getCostOfUse()) {
            return false;
        } else {
            return historyService.getHistoryById(userEntity.getHistoryId()).getCostOfUse() <= ministrationEntity.getMaximumUse();
        }
    }

    public boolean userExistByUserName(String userName) {
        return userRepository.findByUserNameAndIsDeletedFalse(userName) != null;
    }

    public boolean userExistById(String id) {
        return userRepository.findByIdAndIsDeletedFalse(id) != null;
    }
}