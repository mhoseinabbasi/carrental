package com.bontech.carrental.user;

import com.bontech.carrental.exceptionHandling.EmptyFieldException;
import com.bontech.carrental.exceptionHandling.InvalidPasswordException;
import com.bontech.carrental.exceptionHandling.NegativeNumberException;
import com.bontech.carrental.history.HistoryEntity;
import com.bontech.carrental.security.PasswordChecker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping(value = "/create/client")
    public UserEntity createClient(@RequestBody UserEntity userEntity) {
        String userName = userEntity.getUserName();
        String password = userEntity.getPassword();
        int credit = userEntity.getCredit();

        if (userName.isEmpty()) {
            throw new EmptyFieldException("userName");
        }

        if (password.isEmpty()) {
            throw new EmptyFieldException("password");
        }

        if (credit < 0) {
            throw new NegativeNumberException("credit");
        }

        if (!PasswordChecker.checkRegex(password)) {
            throw new InvalidPasswordException("password");
        }

        return userService.createClient(userName, password, credit);
    }

    @PostMapping(value = "/create/admin")
    public UserEntity createAdmin(@RequestBody UserEntity userEntity) {
        String userName = userEntity.getUserName();
        String password = userEntity.getPassword();

        if (userName.isEmpty()) {
            throw new EmptyFieldException("userName");
        }

        if (password.isEmpty()) {
            throw new EmptyFieldException("password");
        }

        if (!PasswordChecker.checkRegex(password)) {
            throw new InvalidPasswordException("password");
        }

        return userService.createAdmin(userName, password);
    }

    @PostMapping(value = "/useministration/{id}")
    public void useMinistration(@PathVariable String id, @RequestParam String ministrationId) {
        if (id.isEmpty()) {
            throw new EmptyFieldException("userId");
        }

        if (ministrationId.isEmpty()) {
            throw new EmptyFieldException("ministrationName");
        }

        userService.useMinistration(id, ministrationId);
    }

    @GetMapping(value = "/userministations/{id}")
    public List<String> getUserDuties(@PathVariable String id) {
        if (id.isEmpty()) {
            throw new EmptyFieldException("userId");
        }

        return userService.getUserDuties(id);
    }

    @GetMapping(value = "/{id}")
    public UserEntity getUser(@PathVariable String id) {
        if (id.isEmpty()) {
            throw new EmptyFieldException("userId");
        }

        return userService.getUser(id);
    }

    @GetMapping(value = "/list")
    public List<UserEntity> getUsers(@RequestParam Integer skip, @RequestParam Integer limit) {
        return userService.getUsers(skip, limit);
    }

    @PutMapping(value = "/changecredit/{id}")
    public UserEntity changeCredit(@RequestParam Integer credit, @PathVariable String id) {
        if (id.isEmpty()) {
            throw new EmptyFieldException("userId");
        }

        if (credit < 0) {
            throw new NegativeNumberException("credit");
        }

        return userService.changeCredit(id, credit);
    }

    @PutMapping(value = "/licensing/{id}")
    public String licensing(@RequestParam String ministrationId, @PathVariable String id) {
        if (id.isEmpty()) {
            throw new EmptyFieldException("userId");
        }

        if (ministrationId.isEmpty()) {
            throw new EmptyFieldException("ministrationName");
        }

        userService.licensing(id, ministrationId);

        return "OK";
    }

    @PutMapping(value = "/cancellicensing/{id}")
    public String cancelLicensing(@RequestParam String ministrationId, @PathVariable String id) {
        if (id.isEmpty()) {
            throw new EmptyFieldException("userId");
        }

        if (ministrationId.isEmpty()) {
            throw new EmptyFieldException("ministrationName");
        }

        userService.cancelLicensing(id, ministrationId);

        return "OK";
    }

    @GetMapping(value = "/userhistories/{id}")
    public List<HistoryEntity> getUserHistories(@PathVariable String id) {
        if (id.isEmpty()) {
            throw new EmptyFieldException("userId");
        }

        return userService.getUserHistories(id);
    }

    @GetMapping(value = "/histories")
    public List<HistoryEntity> getUserHistories() {
        return userService.getHistories();
    }

    @DeleteMapping(value = "/delete/{id}")
    public String delete(@PathVariable String id) {
        if (id.isEmpty()) {
            throw new EmptyFieldException("userId");
        }

        userService.delete(id);

        return "User Deleted";
    }
}