package com.bontech.carrental.user;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<UserEntity, String> {
    UserEntity findByUserNameAndIsDeletedFalse(String userName);

    UserEntity findByIdAndIsDeletedFalse(String id);
}