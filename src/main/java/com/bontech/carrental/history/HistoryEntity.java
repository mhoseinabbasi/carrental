package com.bontech.carrental.history;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Document(collection = "Histories")
public class HistoryEntity {

    @Id
    @Column(nullable = false)
    private String id;

    @Column(nullable = false)
    private String ministrationId;

    @Column(nullable = false)
    private String userId;

    @Column(nullable = false)
    private Integer costOfUse;

    @Column
    private Integer countOfUse;

    @Column
    private Date date;

    @Column(updatable = false)
    @CreationTimestamp
    private Date createdAt;

    @Column
    @UpdateTimestamp
    private Date updatedAt;

    @Column
    private Boolean isDeleted;


    public HistoryEntity() {

    }

    public HistoryEntity(String ministrationId, String userId) {
        this.ministrationId = ministrationId;
        this.userId = userId;
    }

    public String getMinistrationId() {
        return ministrationId;
    }

    public void setMinistrationId(String ministrationId) {
        this.ministrationId = ministrationId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public String getId() {
        return id;
    }


    public Integer getCostOfUse() {
        return costOfUse;
    }

    public void setCostOfUse(Integer costOfUse) {
        this.costOfUse = costOfUse;
    }

    public Integer getCountOfUse() {
        return countOfUse;
    }

    public void setCountOfUse(Integer countOfUse) {
        this.countOfUse = countOfUse;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}