package com.bontech.carrental.history;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryRepository extends MongoRepository<HistoryEntity, String> {
    List<HistoryEntity> findByUserIdAndIsDeletedFalse(String userId);

    HistoryEntity findByIdAndIsDeletedFalse(String id);
}