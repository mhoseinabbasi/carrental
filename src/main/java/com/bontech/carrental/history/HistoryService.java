package com.bontech.carrental.history;

import com.bontech.carrental.exceptionHandling.HistoryNotFoundException;
import com.bontech.carrental.exceptionHandling.UserNotFoundException;
import com.bontech.carrental.user.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class HistoryService {
    @Autowired
    HistoryRepository historyRepository;
    @Autowired
    UserService userService;

    public String create(String ministrationId, String userId, Integer costOfUse) {
        HistoryEntity historyEntity = new HistoryEntity(ministrationId, userId);
        historyEntity.setCostOfUse(costOfUse);
        historyEntity.setCountOfUse(0);
        historyEntity.setDate(new Date());
        historyEntity.setCreatedAt(new Date());
        historyEntity.setUpdatedAt(new Date());
        historyEntity.setDeleted(false);

        historyEntity.setDeleted(false);

        HistoryEntity savedHistoryEntity = historyRepository.save(historyEntity);

        return savedHistoryEntity.getId();
    }

    public void update(String id, Integer countOfUse) {
        HistoryEntity findHistoryEntity = historyRepository.findByIdAndIsDeletedFalse(id);

        findHistoryEntity.setCountOfUse(countOfUse + 1);
        findHistoryEntity.setUpdatedAt(new Date());

        historyRepository.save(findHistoryEntity);
    }

    public List<HistoryEntity> getHistories() {
        List<HistoryEntity> histories = historyRepository.findAll();

        if (histories.size() == 0) {
            throw new HistoryNotFoundException();
        }

        return histories;
    }

    public List<HistoryEntity> getHistoriesByUserId(String userId) {
        if (!userService.userExistById(userId)) {
            throw new UserNotFoundException(userId);
        }

        List<HistoryEntity> histories = historyRepository.findByUserIdAndIsDeletedFalse(userId);

        if (histories.size() == 0) {
            throw new HistoryNotFoundException();
        }

        return histories;
    }

    public HistoryEntity getHistoryById(String id) {
        return historyRepository.findByIdAndIsDeletedFalse(id);
    }

    public boolean isToday(String id) {
        if (id != null) {
            HistoryEntity historyEntity = historyRepository.findByIdAndIsDeletedFalse(id);

            return historyEntity.getDate().getDay() == new Date().getDay();
        }
        return false;
    }
}