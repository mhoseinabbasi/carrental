package com.bontech.carrental;

import com.bontech.carrental.history.HistoryRepository;
import com.bontech.carrental.ministration.MinistrationRepository;
import com.bontech.carrental.user.UserRepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackageClasses = {
        UserRepository.class,
        MinistrationRepository.class,
        HistoryRepository.class
})

@SpringBootApplication
public class CarRentalApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarRentalApplication.class, args);
    }
}
