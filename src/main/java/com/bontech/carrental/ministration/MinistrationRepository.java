package com.bontech.carrental.ministration;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MinistrationRepository extends MongoRepository<MinistrationEntity, String> {
    MinistrationEntity findByIdAndIsDeletedFalse(String id);

    MinistrationEntity findByNameAndIsDeletedFalse(String name);
}