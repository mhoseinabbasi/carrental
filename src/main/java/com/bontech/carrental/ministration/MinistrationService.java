package com.bontech.carrental.ministration;

import com.bontech.carrental.exceptionHandling.ExistedException;
import com.bontech.carrental.exceptionHandling.InvalidStatusException;
import com.bontech.carrental.exceptionHandling.MinistrationsNotFoundException;
import com.bontech.carrental.exceptionHandling.NegativeNumberException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MinistrationService {

    @Autowired
    MinistrationRepository ministrationRepository;
    @Autowired
    MongoTemplate mongoTemplate;

    public MinistrationEntity create(String name, Integer costOfUse, Integer maximumUse) {
        if (ministrationExistByName(name)) {
            throw new ExistedException("ministration", String.format("ministration name with name %s existed", name));
        }

        MinistrationEntity ministrationEntity = new MinistrationEntity(name, costOfUse, maximumUse);
        ministrationEntity.setStatus(MinistrationEntity.Status.OFF);
        ministrationEntity.setCreatedAt(new Date());
        ministrationEntity.setUpdatedAt(new Date());
        ministrationEntity.setDeleted(false);

        return ministrationRepository.save(ministrationEntity);
    }

    public MinistrationEntity getMinistration(String id) {
        if (!ministrationExistById(id)) {
            throw new MinistrationsNotFoundException(id);
        }

        return ministrationRepository.findByIdAndIsDeletedFalse(id);
    }

    public MinistrationEntity getMinistrationByName(String name) {
        if (!ministrationExistByName(name)) {
            throw new MinistrationsNotFoundException(name);
        }

        return ministrationRepository.findByNameAndIsDeletedFalse(name);
    }

    public List<MinistrationEntity> getMinistrations(Integer skip, Integer limit) {
        Query query = new Query(Criteria.where("isDeleted").is(false));

        List<MinistrationEntity> ministrationEntityList = mongoTemplate.find(query.skip(skip).limit(limit), MinistrationEntity.class);

        if (ministrationEntityList.size() == 0) {
            throw new MinistrationsNotFoundException();
        }

        return ministrationEntityList;
    }

    public MinistrationEntity update(String id, MinistrationEntity ministrationEntity) {
        if (!ministrationExistById(id)) {
            throw new MinistrationsNotFoundException(id);
        }

        MinistrationEntity findMinistrationEntity = ministrationRepository.findByIdAndIsDeletedFalse(id);

        if (ministrationEntity.getName() != null && !ministrationEntity.getName().isEmpty()) {
            if (ministrationExistByName(ministrationEntity.getName())) {
                throw new ExistedException("ministration", String.format("ministration name with name %s existed", ministrationEntity.getName()));
            }

            findMinistrationEntity.setName(ministrationEntity.getName());
        }

        if (ministrationEntity.getCostOfUse() != null) {
            if (ministrationEntity.getCostOfUse() < 0) {
                throw new NegativeNumberException("costOfUse");
            }

            findMinistrationEntity.setCostOfUse(ministrationEntity.getCostOfUse());
        }

        if (ministrationEntity.getMaximumUse() != null) {
            if (ministrationEntity.getMaximumUse() < 0) {
                throw new NegativeNumberException("maximumUse");
            }

            findMinistrationEntity.setMaximumUse(ministrationEntity.getMaximumUse());
        }

        findMinistrationEntity.setUpdatedAt(new Date());

        return ministrationRepository.save(findMinistrationEntity);
    }

    public void changeStatus(String id, MinistrationEntity.Status status) {
        if (!ministrationExistById(id)) {
            throw new MinistrationsNotFoundException(id);
        }

        MinistrationEntity findMinistrationEntity = ministrationRepository.findByIdAndIsDeletedFalse(id);

        if (status.equals(MinistrationEntity.Status.ON) && todayIsFree(id)) {
            findMinistrationEntity.setStartTime(new Date());
        } else if (status.equals(MinistrationEntity.Status.OFF)) {
            findMinistrationEntity.setEndTime(new Date());
        } else {
            throw new InvalidStatusException();
        }

        findMinistrationEntity.setStatus(status);
        findMinistrationEntity.setUpdatedAt(new Date());

        ministrationRepository.save(findMinistrationEntity);
    }

    public void delete(String id) {
        if (!ministrationExistById(id)) {
            throw new MinistrationsNotFoundException(id);
        }

        MinistrationEntity findMinistrationEntity = ministrationRepository.findByIdAndIsDeletedFalse(id);

        findMinistrationEntity.setDeleted(true);

        ministrationRepository.save(findMinistrationEntity);
    }

    public boolean todayIsFree(String id) {
        MinistrationEntity ministrationEntity = ministrationRepository.findByIdAndIsDeletedFalse(id);

        if (ministrationEntity.getStatus().equals(MinistrationEntity.Status.ON)) {
            return false;
        } else {
            return ministrationEntity.getEndTime() == null || ministrationEntity.getStartTime() == null || ministrationEntity.getEndTime().getDay() != new Date().getDay();
        }
    }

    public boolean ministrationExistById(String id) {
        return ministrationRepository.findByIdAndIsDeletedFalse(id) != null;
    }

    public boolean ministrationExistByName(String name) {
        return ministrationRepository.findByNameAndIsDeletedFalse(name) != null;
    }
}