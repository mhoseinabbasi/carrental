package com.bontech.carrental.ministration;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Document(collection = "Ministration")
public class MinistrationEntity {

    @Id
    @Column(nullable = false)
    private String id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private Integer costOfUse;

    @Column(nullable = false)
    private Integer maximumUse;

    @Column(nullable = false)
    private Status status;

    @Column()
    private Date startTime;

    @Column()
    private Date endTime;

    @Column(updatable = false)
    @CreationTimestamp
    private Date createdAt;

    @Column
    @UpdateTimestamp
    private Date updatedAt;

    @Column
    private Boolean isDeleted;

    public enum Status {
        ON,
        OFF
    }

    public MinistrationEntity() {
    }

    public MinistrationEntity(String name, Integer costOfUse, Integer maximumUse) {
        this.name = name;
        this.costOfUse = costOfUse;
        this.maximumUse = maximumUse;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCostOfUse() {
        return costOfUse;
    }

    public void setCostOfUse(Integer costOfUse) {
        this.costOfUse = costOfUse;
    }

    public Integer getMaximumUse() {
        return maximumUse;
    }

    public void setMaximumUse(Integer maximumUse) {
        this.maximumUse = maximumUse;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}