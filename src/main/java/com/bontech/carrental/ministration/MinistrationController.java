package com.bontech.carrental.ministration;

import com.bontech.carrental.exceptionHandling.EmptyFieldException;
import com.bontech.carrental.exceptionHandling.InvalidStatusException;
import com.bontech.carrental.exceptionHandling.NegativeNumberException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/ministration")
public class MinistrationController {

    @Autowired
    MinistrationService ministrationService;

    @PostMapping(value = "/create")
    public MinistrationEntity create(@RequestBody MinistrationEntity ministrationEntity) {
        String name = ministrationEntity.getName();
        Integer costOfUse = ministrationEntity.getCostOfUse();
        Integer maximumUse = ministrationEntity.getMaximumUse();

        if (name.isEmpty()) {
            throw new EmptyFieldException("ministrationName");
        }

        if (costOfUse < 0) {
            throw new NegativeNumberException("costOfUse");
        }

        if (maximumUse < 0) {
            throw new NegativeNumberException("maximumUse");
        }

        return ministrationService.create(name, costOfUse, maximumUse);
    }

    @GetMapping(value = "/{id}")
    public MinistrationEntity getMinistration(@PathVariable String id) {
        if (id.isEmpty()) {
            throw new EmptyFieldException("ministrationId");
        }

        return ministrationService.getMinistration(id);
    }

    @GetMapping(value = "/list")
    public List<MinistrationEntity> getMinistrations(@RequestParam Integer skip, @RequestParam Integer limit) {
        return ministrationService.getMinistrations(skip, limit);
    }

    @PutMapping(value = "/update/{id}")
    public MinistrationEntity update(@RequestBody MinistrationEntity ministrationEntity, @PathVariable String id) {
        if (id.isEmpty()) {
            throw new EmptyFieldException("ministrationId");
        }

        return ministrationService.update(id, ministrationEntity);
    }

    @PutMapping(value = "/changestatus/{id}")
    public String changeStatus(@RequestParam String status, @PathVariable String id) {
        if (id.isEmpty()) {
            throw new EmptyFieldException("ministrationId");
        }

        MinistrationEntity.Status finalStatus;

        if (status.equalsIgnoreCase(MinistrationEntity.Status.ON.name())) {
            finalStatus = MinistrationEntity.Status.ON;
        } else if (status.equalsIgnoreCase(MinistrationEntity.Status.OFF.name())) {
            finalStatus = MinistrationEntity.Status.OFF;
        } else {
            throw new InvalidStatusException(status);
        }

        ministrationService.changeStatus(id, finalStatus);

        return "Ministration Changed Status To " + status.toUpperCase();
    }

    @DeleteMapping(value = "/delete/{id}")
    public String delete(@PathVariable String id) {
        if (id.isEmpty()) {
            throw new EmptyFieldException("ministrationId");
        }

        ministrationService.delete(id);

        return "Ministration Deleted";
    }
}