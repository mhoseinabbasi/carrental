package com.bontech.carrental.exceptionHandling;

public class HistoryNotFoundException extends RuntimeException {
    private final String message;

    public HistoryNotFoundException(String id) {
        message = String.format("History with id %s not found!", id);
    }

    public HistoryNotFoundException() {
        message = "Histories not found!";
    }


    @Override
    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return "HISTOR_NOT_FOUND";
    }
}
