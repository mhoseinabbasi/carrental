package com.bontech.carrental.exceptionHandling;

public class NotPermissionException extends RuntimeException {
    private final String message;

    public NotPermissionException() {
        message = "This time the service is not permission!";
    }


    @Override
    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return "NOT_PERMISSION";
    }
}
