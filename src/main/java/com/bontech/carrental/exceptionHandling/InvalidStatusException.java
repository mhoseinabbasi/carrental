package com.bontech.carrental.exceptionHandling;

public class InvalidStatusException extends RuntimeException {
    private final String title;
    private final String message;

    public InvalidStatusException(String filed) {
        title = filed.toUpperCase() + "_REQUIRED";
        message = filed.toLowerCase()
                .replace("_", " ")
                .replace("-", " ")
                + " is Invalid, Please enter a valid status : \"ON\" or \"OFF\"!";
    }

    public InvalidStatusException() {
        title = "CHANGE_REQUIRED";
        message = "Can not change status to \"ON\"";
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return title;
    }


}
