package com.bontech.carrental.exceptionHandling;

public class EmptyFieldException extends RuntimeException {
    private final String title;
    private final String message;

    public EmptyFieldException(String filed) {
        title = filed.toUpperCase() + "_REQUIRED";
        message = filed.toLowerCase()
                .replace("_", " ")
                .replace("-", " ")
                + " can not empty!";
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return title;
    }


}
