package com.bontech.carrental.exceptionHandling;

public class InternalServerException extends RuntimeException {

    public InternalServerException() {

    }

    public String getTitle() {
        String title = "Internal Server Error";
        return title;
    }

}
