package com.bontech.carrental.exceptionHandling;

public class ExistedException extends RuntimeException {
    private final String title;
    private final String message;

    public ExistedException(String filed, String message) {
        title = filed.toUpperCase() + "_EXISTED";
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return title;
    }
}