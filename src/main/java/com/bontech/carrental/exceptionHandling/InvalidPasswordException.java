package com.bontech.carrental.exceptionHandling;

public class InvalidPasswordException extends RuntimeException {
    private final String title;
    private final String message;

    public InvalidPasswordException(String filed) {
        title = filed.toUpperCase() + "_REQUIRED";
        message = filed.toLowerCase()
                .replace("_", " ")
                .replace("-", " ")
                + " is Invalid, please add Strong Password!";
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return title;
    }


}
