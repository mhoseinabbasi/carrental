package com.bontech.carrental.exceptionHandling;

public class InvalidCreditException extends RuntimeException {
    private final String title;
    private final String message;

    public InvalidCreditException(String filed) {
        title = filed.toUpperCase() + "_REQUIRED";
        message = filed.toLowerCase()
                .replace("_", " ")
                .replace("-", " ")
                + " is Invalid, Please enter a number greater than '100'!";
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return title;
    }


}
