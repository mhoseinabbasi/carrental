package com.bontech.carrental.exceptionHandling;

public class UserNotFoundException extends RuntimeException {
    private final String message;

    public UserNotFoundException(String userId) {
        message = String.format("User with id %s not found!", userId);
    }

    public UserNotFoundException() {
        message = "User not found!";
    }


    @Override
    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return "USER_NOT_FOUND";
    }
}
