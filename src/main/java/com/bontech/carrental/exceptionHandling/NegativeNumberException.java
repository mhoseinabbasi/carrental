package com.bontech.carrental.exceptionHandling;

public class NegativeNumberException extends RuntimeException {
    private final String title;
    private final String message;

    public NegativeNumberException(String filed) {
        title = filed.toUpperCase() + "_REQUIRED";
        message = filed.toLowerCase()
                .replace("_", " ")
                .replace("-", " ")
                + " can not Negative!";
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return title;
    }
}
