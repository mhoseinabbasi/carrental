package com.bontech.carrental.exceptionHandling;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(value = EmptyFieldException.class)
    public ResponseEntity<Object> emptyFieldException(EmptyFieldException exception) {
        return new ResponseEntity<>(exception.getTitle() + " : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = NegativeNumberException.class)
    public ResponseEntity<Object> negativeNumberException(NegativeNumberException exception) {
        return new ResponseEntity<>(exception.getTitle() + " : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = InvalidPasswordException.class)
    public ResponseEntity<Object> invalidPasswordException(InvalidPasswordException exception) {
        return new ResponseEntity<>(exception.getTitle() + " : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ExistedException.class)
    public ResponseEntity<Object> existedException(ExistedException exception) {
        return new ResponseEntity<>(exception.getTitle() + " : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = InvalidCreditException.class)
    public ResponseEntity<Object> invalidCreditException(InvalidCreditException exception) {
        return new ResponseEntity<>(exception.getTitle() + " : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = InternalServerException.class)
    public ResponseEntity<Object> internalServerException(InternalServerException exception) {
        return new ResponseEntity<>(exception.getTitle(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = UserNotFoundException.class)
    public ResponseEntity<Object> userNotFoundException(UserNotFoundException exception) {
        return new ResponseEntity<>(exception.getTitle() + " : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = MinistrationsNotFoundException.class)
    public ResponseEntity<Object> DutiesNotFoundException(MinistrationsNotFoundException exception) {
        return new ResponseEntity<>(exception.getTitle() + " : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = InvalidStatusException.class)
    public ResponseEntity<Object> invalidStatusException(InvalidStatusException exception) {
        return new ResponseEntity<>(exception.getTitle() + " : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = HistoryNotFoundException.class)
    public ResponseEntity<Object> historyNotFoundException(HistoryNotFoundException exception) {
        return new ResponseEntity<>(exception.getTitle() + " : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = NotPermissionException.class)
    public ResponseEntity<Object> notPermissionException(NotPermissionException exception) {
        return new ResponseEntity<>(exception.getTitle() + " : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }
}