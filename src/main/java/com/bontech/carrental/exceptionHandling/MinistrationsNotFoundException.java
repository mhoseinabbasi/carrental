package com.bontech.carrental.exceptionHandling;

public class MinistrationsNotFoundException extends RuntimeException {
    private final String message;

    public MinistrationsNotFoundException(String id) {
        message = String.format("Ministration with id %s not found!", id);
    }

    public MinistrationsNotFoundException() {
        message = "Ministrations not found!";
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return "MINISTRATION_NOT_FOUND";
    }
}
